#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'me'
SITENAME = 'Framaforms'
SITEURL = ''
RELATIVE_URLS = False
DISPLAY_CATEGORIES_ON_MENU = True

PATH = 'content'
OUTPUT_PATH = 'public'

THEME = 'theme/pelican-clean-blog'
THEME_STATIC_DIR = 'theme'

FAVICON = 'images/icons/favicon.ico'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'fr'
DATE_FORMATS = {
    'en': '%m/%d/%Y',
    'fr': '%d/%m/%Y',
}
DEFAULT_DATE_FORMAT = '%a %d %B %Y'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DEFAULT_ORPHANS = 0
PAGINATED_TEMPLATES = {'category': 5}
DEFAULT_PAGINATION = 5

PLUGIN_PATHS = ['./plugins']
PLUGINS = ['i18n_subsites']

I18N_SUBSITES = {
    'en': {
        'SITENAME': 'Framaforms',
    },
  }

I18N_UNTRANSLATED_ARTICLES = 'hide'
CATEGORIES_I18N = {
    'News': {
        'fr': 'Actualités',
        'en': 'News'
    }
}


# Custom Jinja2 filter to get the Index page from Markdown
# From https://siongui.github.io/2016/02/19/pelican-generate-index-html-by-rst-or-md/
def hidden_pages_get_page_with_slug_index(hidden_pages):
    for page in hidden_pages:
        if page.slug == "index":
            return page

JINJA_FILTERS = {
    "hidden_pages_get_page_with_slug_index": hidden_pages_get_page_with_slug_index,
}
