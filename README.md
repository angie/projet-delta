# Projet Delta

Mysterious project

Makes use of [**Pelican**](https://docs.getpelican.com/en/stable/) and its [i18n module](https://github.com/getpelican/pelican-plugins/tree/master/i18n_subsites/). Some icons are from [Bootstrap](https://icons.getbootstrap.com/#icons).

The theme is adapted from [Pelican Clean Blog](https://github.com/gilsondev/pelican-clean-blog/tree/ea156f8f1741e473bc0ab848b7c8898112d6ffb5)

## Dev

Run locally with : `pelican --autoreload --listen` or `make devserver`

Make for prod (RSS feeds and such) with `make publish`

## Adding a page
To add a new page to the website, follow these instructions :
1. create a folder under `content/pages`. For example : `Weather`.
2. create a Markdown file under this folder for the English page : `weather-en.md`.
3. At the top of the MD file, insert the following metadata :
```
Title: My page title (will be displayed at the top of the page and in the tab)
Authors: me (doesn't matter, but Pelican needs it)
Date: 2020-06-14 (doesn't matter, but Pelican needs it)
Modified: 2020-06-14 (doesn't matter, but Pelican needs it)
Slug: faq (must be lowercase with hyphens (-) between words : this is used for the URL)
Lang: fr (language code. Same as in the filename)
Nomenu: True (Insert this if you don't want the page to be listed in the navbar. Otherwise, delete this line.)
```
4. Write your page, full Markdown.
5. Profit !


# TODO
- [ ] modifier les instructions pour traduire un site dans /install.html

