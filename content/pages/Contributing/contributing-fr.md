Title: Contribuer
Authors: moi
Date: 2020-06-14
Modified: 2020-06-14
Slug: contributing
Lang: fr

Si vous réfléchissez à contribuer à Framaforms, déjà **merci beaucoup** !

# Espaces de discussions

Framaforms est un **logiciel libre** ouvert aux contributions et aux suggestions.

Pour cela, nous échangeons via le [forum Framacolibri](https://framacolibri.org/), qui nous permet d'échanger sur des propositions d'améliorations, de relever des bugs, etc. Chacun est libre de s'y inscrire, et d'ouvrir un sujet (en précisant bien que la discussion porte sur **Framaforms**,  le forum aborde de nombreux autres sujets !)

 Le [repo Framagit](https://framagit.org/framasoft/framaforms) donne également l'accès au code et aux gestions de tickets (_issues_). Si vous maîtrisez cet outil, vous êtes les bienvenu⋅es sur cet espace.

# Code de conduite

Afin que les espaces de contribution restent ouverts et inclusifs pour tou⋅te⋅s, nous vous demandons de respecter les quelques points suivants :

* Faire preuve de politesse et de patience.
* Refuser et signaler toute forme de discrimination, que ce soit raciale, sexiste, homophobe, validiste ou autre.
* Éviter toute condescendance de type « techbro » : ce n'est pas parce que vous avez des compétences techniques que vous avez raison, ni que vous êtes la personne la plus pertinente sur tous les sujets.

# Axes de contribution

**Pas besoin de savoir coder pour contribuer !**

Voici une liste des tâches utiles auxquelles vous pouvez vous atteler sans compétence en informatique particulière :

* **Traduire** (![État de la traduction](https://weblate.framasoft.org/widgets/framaforms/-/svg-badge.svg)) : Nous essayons de rendre le logiciel disponible au plus de personnes possibles, quelque soit leur langue natale. Nous collaborons pour cela via la plateforme de traduction [Weblate](https://weblate.framasoft.org/engage/framaforms/).
* **Documenter** :  **todo avec les pages de doc**
* **Repenser** : Le logiciel a besoin ~~d'un sérieux ravalement de façade~~ d'une couche de peinture neuve. Les options de création d'un formulaire sont nombreuses et prêtent régulièrement à confusion. Si l'UX et UI vous intéressent, pourquoi ne pas réfléchir à faire les choses autrement ?
* **Répondre** : Les utilisateur⋅ices de Framaforms sont nombreu⋅ses⋅x, et ont parfois besoin d'aide. Faites-leur profiter de votre expérience de Framaforms en leur répondant sur [le forum](https://framacolibri.org/). Certaines [issues](https://framagit.org/framasoft/framaforms/-/issues) demandent également une discussion préalable pour faire évoluer le logiciel dans la bonne direction, toute participation à la discussion est bienvenue.



Si vous avez des **compétences techniques**, vous pouvez également contribuer au code de Framaforms par les moyens suivants :

* **Travailler sur une issue ouverte** : si c'est votre première contribution à Framaforms, n'hésitez pas à vous attrivuer les issues labellées `🤏 easy`, qui sont parfaite pour un début. N'hésitez pas également à **participer aux discussions** en cours sous les issues pour donner votre point de vue sur les différentes options techniques.
* **Écrire des tests** : étant basé sur Drupal 7, Framaforms peut utiliser le [framework de tests fonctionnels natif de Drupal 7](https://www.drupal.org/docs/7/testing/simpletest-testing-tutorial-drupal-7). Développer des tests pourrait permettre de tester automatiquement les fonctionnalités de Framaforms avant toute action de `merge`, ce qui mènerait à un processus de développement plus rigoureux. Isoler une fonctionnalité et développer un test spécifique serait d'une grande aide.
