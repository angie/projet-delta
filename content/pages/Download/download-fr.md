Title: Télécharger
Authors: moi
Date: 2020-06-14
Modified: 2020-06-14
Slug: download
Nomenu: True
Lang: fr

## Télécharger la dernière version de Drupal 7

Pour cela, il faut se rendre sur [le site de Drupal 7](https://www.drupal.org/project/drupal/releases/7.73).

## Télécharger le profil d'installation de Framaforms

Télécharger la dernière version officielle du profil [ici]({static}../../static/framaforms_profile_1.0.3.zip), ou télécharger la version de développement en clonant [le repo Git](https://framagit.org/framasoft/framaforms/), puis en copiant le sous-dossier `profiles/framaforms_org`.
