Title: Download
Authors: moi
Date: 2020-06-14
Modified: 2020-06-14
Slug: download
Nomenu: True
Lang: en

## Download the latest version of Drupal 7

You can do that by [clicking here](https://www.drupal.org/project/drupal/releases/7.73).

## Download the Framaforms installation profile

Get the latest official release [here]({static}../../static/framaforms_profile_1.0.3.zip), or get the latest, bleeding-edge version by copying the `profile/framaforms_org` on [the Git repo](https://framagit.org/framasoft/framaforms/).
