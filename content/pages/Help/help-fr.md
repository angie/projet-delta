Title: Aide
Authors: moi
Date: 2020-06-14
Modified: 2020-06-14
Slug: help
Lang: fr

# Un problème ? un doute ?
**Pas de panique !**
Dans 95 % des cas, vous trouverez une solution dans [la FAQ]({filename}../FAQ/faq-fr.md).

# Toujours pas ?
Si vous ne trouvez pas votre bonheur dans la FAQ, elle se trouvera sans doute dans [la documentation](https://docs.framasoft.org/fr/framaforms).

# Décidément pas ?
Pas de réponse ? Rien de rien ?

Dans ce cas, faites appel à la communauté sur [le forum Framacolibri](https://framacolibri.org/), lieu d'échange autour de Framaforms et de nombreux autres logiciels libre.
