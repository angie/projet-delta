Title: Help
Authors: moi
Date: 2020-06-14
Modified: 2020-06-14
Slug: help
Lang: en

# Having a problem ?
**Don't panic !**
In the vast majority of cases, you'll find an answer in [the FAQ page]({filename}../FAQ/faq-en.md).

# Didn't work ?
If it's not in the FAQ, your answer will no doubt be in  [the documentation](https://docs.framasoft.org/en/framaforms).

# Still here ?
Nothing ? Nada ?

Get in touch with the community [on Framacolibri](https://framacolibri.org/), a forum to share thoughts and experience on Framaforms and other free and libre software.
