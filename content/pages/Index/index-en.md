Title: Welcome
Authors: moi
Date: 2020-06-14
Modified: 2020-06-14
Status: hidden
Slug: index
Lang: en

# ![Checkboxes icon]({filename}../../images/icons/checks.svg) What is Framaforms ?

Framaforms allows you to create and share forms online.

It has many features to analyze results, interact by email with users, various themes...

[Try it here ! »](https://framaforms.org)

---
# ![People icon]({filename}../../images/icons/people.svg) Free software

Framaforms is free libre software, respectful of your personal data and private life. It is under [AGPL 2.0](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html).

It was originally developped by [Framasoft](https://framasoft.org/en/) during the [De-google-ify Internet campaign](https://degooglisons-internet.org/en/).

You can [check its source code here](https://framagit.org/framasoft/framaforms).

---
# ![Droplet icon]({filename}../../images/icons/droplet.svg) Drupal 7

Framaforms makes use of [Drupal 7](https://www.drupal.org/drupal-7.0), the famous and heavily used CMS, using a lot of its core feature and modules.

Other features were developped specifically for Framaforms.

Drupal is also free software, placed under [GPL v3.0](https://www.gnu.org/licenses/quick-guide-gplv3.html).

---
# ![Forward icon]({filename}../../images/icons/forward.svg) Installing Framaforms

**Host your own Framaforms !** ([They]({filename}../People/they-use-en.md) did it !)

Take full control of your data and install your own instance of Framaforms, for your own community to use. Framaforms is very flexible, and you can easily adapt it to your needs.

All you'll need is a web-accessible server, a [PostgreSQL database](https://www.postgresql.org/) , and some coffee.

[Get started » ](pages/install.html)

