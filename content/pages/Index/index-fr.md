Title: Bienvenue
Authors: moi
Date: 2020-06-14
Modified: 2020-06-14
Status: hidden
Slug: index
Lang: fr

# ![Checkboxes icon]({filename}../../images/icons/checks.svg) Framaforms, c'est quoi ?

Framaforms vous permet de créer et partager des formulaires en ligne.

Il dispose de nombreuses fonctionnalités permettant d'analyser les résultats, d'intéragir par mail avec les répondant⋅e⋅s, de personnaliser l'apparence des formulaires...

[Essayer Framaforms »](https://framaforms.org)

---
# ![People icon]({filename}../../images/icons/people.svg) Logiciel libre

Framaforms est un logiciel libre et gratuit, respectueux de vos données personnelles et de votre vie privée. Il est placé sous [License AGPL 2.0](https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html).

Il a été développé par [l'association Framasoft](https://framasoft.org) dans le cadre de la campagne [Degooglisons Internet](https://degooglisons-internet.org/fr/).

Vous pouvez également [consulter son code source](https://framagit.org/framasoft/framaforms).

---
# ![Droplet icon]({filename}../../images/icons/droplet.svg) Drupal 7

Framaforms utilise toute la puissance de [Drupal 7](https://www.drupal.org/drupal-7.0/fr), le fameux CMS utilisé massivement partout sur Internet, et intègre de nombreuses fonctionnalités de son cœur et de ses modules.

D'autres modules ont été développé spécifiquement pour Framaforms.

Drupal est également un logiciel libre, placé sous [GPL v3.0](https://www.gnu.org/licenses/quick-guide-gplv3.html).

---
# ![Forward icon]({filename}../../images/icons/forward.svg) Installer Framaforms

**Vous aussi, hébergez Framaforms !** ([Eux]({filename}../People/they-use-fr.md) l'ont fait !)

Afin de maîtriser totalement vos données et vos usages, vous pouvez installer votre propre instance de Framaforms, à l'intention de votre propre communauté. Hautement configurable, vous pourrez facilement adapter Framaforms à vos propre besoins.

Vous aurez pour cela besoin d'un serveur accessible par Internet, d'une [base de données PostgreSQL](https://www.postgresql.org/), et d'un peu de café.

[En savoir plus » ](pages/install.html)
