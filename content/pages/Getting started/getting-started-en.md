Title: Getting started
Authors: moi
Date: 2020-06-14
Modified: 2020-06-14
Slug: getting-started
Lang: en

# Getting to know Framaforms

Framaforms allows you to easily create and share forms online, but also to lookup and analyze submissions, interact by email with your public, and much more.

Framaforms is [free libre software](https://framasoft.org/en/full#topPglogiciel) and respects your personal data. Your information as well as your public's will be accessible by no one but you.

The best way to get to know Framaforms better and to figure if it fits your needs is to **try it**. Firstly, register on [Framaforms.org](https://framaforms.org), create your first form by clicking on "New form" and ... follow the flow !

# Having a problem ?

If you encounter a problem using Framaforms, you'll find answers on [our help page]({filename}../Help/help-en.md).

# A step further

If you still have questions, you can also :

* check our [our advanced documentation](https://docs.framasoft.org/fr/framaforms/) (French only, for now),
* ask your question on [Framacolibri, our forum](https://framacolibri.org/).
