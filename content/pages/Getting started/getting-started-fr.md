Title: Premiers pas
Authors: moi
Date: 2020-06-14
Modified: 2020-06-14
Slug: getting-started
Lang: fr

# Découvrir Framaforms

Framaforms est un logiciel permettant de facilement créer et partager des formulaires en ligne, mais aussi visualiser et analyser les résultats, interagir par mail avec les répondant⋅es, et bien plus.

Framaforms est un [logiciel libre](https://framasoft.org/fr/full/#topPglogiciel) et respectueux de votre vie privée. Vos informations personnelles et celles des personnes qui répondront à vos formulaires ne seront accessibles par nul autre que vous.

Le meilleur moyen de savoir si Framaforms correspondra à vos besoins, c'est de **l'essayer**. Pour commencer, créez un compte sur [Framaforms.org](https://framaforms.org/), créez votre premier formulaire en cliquant sur « Créer un formulaire vierge »... et laissez-vous guider !

# Un problème ?

Vous rencontrez un problème dans votre utilisation de Framaforms ?

Vous trouverez des réponses sur [notre page d'aide]({filename}../Help/help-fr.md).

# Pour aller plus loin

Vous ne trouvez toujours pas votre bonheur ? Alors vous pouvez également :

* consulter notre documentation avancée [à cette adresse](https://docs.framasoft.org/fr/framaforms/),
* poser une question [directement sur le forum Framacolibri](https://framacolibri.org/).
