Title: Installer
Authors: moi
Date: 2020-06-14
Modified: 2020-06-14
Slug: install
Lang: fr

# Prérequis

* **PHP 7.3**, d'après les recommandations de Drupal 7, avec les modules suivants : `mb_string`, `pgsql` and `dom` (téléchargeables avec la commande `sudo apt-get install php7.3-yourmodule` sur les systèmes Debian-based). Le module `curl` est également pour les sites de développement et de tests.
* **NGINX**. Peut également fonctionner avec Apache, mais nous n'avons pas essayé. Si vous arrivez à configurer un site Framaforms avec Apache, prévenez-nous !
* **PostgreSQL 9.4.12** ou plus recent.

_**Note** : PostgreSQL peut sembler un choix étrange car Drupal 7 supporte également MySQL et MariaDB. Ce choix a été fait pour des raisons de performance, MySQL ne s'adaptant pas à des instances fortement utilisées. La plupart des modules fonctionneront également pour MySQL, mais certaines requêtes pourront échouer, notamment celles qui concernent [l'expiration des formulaires](https://framagit.org/framasoft/framaforms/-/wikis/Expiration-of-webforms). Si votre site Framaforms fonctionne avec MySQL, prévenez-nous également !_ :wink:

# Préparation

Tout d'abord, configurez votre **reverse proxy** pour rendre Drupal accessible.

Voici une configuration NGINX fonctionnelle :

```
server {
    listen 80;
    listen [::]:80;

    root /var/www/your_folder;
    index index.html;

    location ~ \.php$ {
        try_files $uri =404;
        include /etc/nginx/fastcgi_params;
        fastcgi_send_timeout 300s;
        fastcgi_read_timeout 300s;
        fastcgi_pass unix:/run/php/php7.3-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    }

    error_log /var/log/nginx/<yoursite>.error.log;
	access_log /var/log/nginx/<yoursite>.access.log combined;

    location / {
        autoindex on ;
        autoindex_exact_size off;
        try_files $uri index.html /index.php?$query_string;
    }
}
```

**NB : il vous faudra [ajouter votre configuration SSL](https://www.nginx.com/blog/using-free-ssltls-certificates-from-lets-encrypt-with-nginx/) à cette configuration NGINX pour compléter cette étape.** Vous pouvez vérifier votre configuration avec la commande `nginx -t`.

C'est tout bon ? Alors passons à l'installation du site !

# Étapes d'installation

1. **Téléchargez la dernière version de Drupal 7** (voir la [page d'installation]({filename}../Download/download-fr.md)).
2. **Placez la distribution dans votre racine HTTP.** Cela peut être n'importe où sur votre serveur, mais doit correspondre à votre configuration NGINX. Si vous utilisez la configuration ci-dessus, placez-le dans `/var/www/framaforms`/.
3. **Téléchargez le profil d'installation Framaforms** en suivant [ces instructions]({filename}../Download/download-en.md), et le copier dans le dossier `profiles`. Vous devriez donc avoir un dossier `profiles/framaforms_org` contenant lui-même une arborescence de dossier.
4. **Créez votre base de données** puis votre **fichier `settings.php**s` comme indiqué sur la [documentation Drupal](https://www.drupal.org/docs/7/install/step-2-create-the-database).
5. **Rendez-vous sur le script d'installation.** Naviguez sur votre site à l'adresse `<votreurl.tdl>/install.php `. Vous trouverez là les instructions pour mettre en place votre instance. Patientez pendant que tout s'installe, puis...

![Installation menu]({static}../../images/install_installation_pannel.png)

6. **Renseignez les informations générales de votre site**. Vous devez indiquer le nom de votre site, une adresse mail de contact pour l'administrateur⋅ice, et des informations pour le compte d'administration. Vous utiliserez ce dernier pour la gestion du site, donc mémorisez-bien le nom d'utilisateur et le mot de passe !

![Configuration pannel]({static}../../images/install_config_pannel.png)

7. Validez le formulaire, et vous rendre sur la page d'accueil du site. **Vous y êtes presque !** Plus que deux choses.
8. **Activez la feature Framaforms**. Cliquez sur « Modules » dans la barre d'administration en haut de page (ou rendez-vous directement sur `<votreurl.tdl>/admin/modules`), trouvez la section `framaforms_feature`, cochez la case correspondante et **enregistrez votre configuration** au moyen du bouton en bas de page.

![Activate feature menu]({static}../../images/install_save_feature.png)

9. **(optionnel) Créez les pages par défaut.** Framaforms fournit des modèles pour les pages d'accueil, les pages d'erreur 404/403, la documentation utilisateur, etc. Pour les générer, rendez-vous sur l'URL `<votresite.tdl>/admin/config/system/framaforms` et cliquez sur le bouton "Créez les pages par défaut".

Votre site devrait maintenant être fonctionnel !

# Paramètres avancés (optionnel)

Comme Framaforms est basé sur Drupal 7, sa configuration est très facilement modifiable en accédant aux menus d'administration des différents modules.

Parmi les modifications que vous pourriez y apporter, il y a par exemple :

* **Cacher les erreurs** : les erreurs du site, les avertissements et les messages sont par défaut visibles par tous les utilisateurs, mais vous souhaitez peut-être qu'ils ne le soient pas. Pour celà, naviguez à l'adresse `/admin/people/permissions`et désactivez les lignes correspondantes pour les utilisateur⋅ices authentifé⋅es et / ou les utilisateur⋅ices anonymes ("Voir les messages d'erreur", "Voir les messages d'avertissement").
* **Changer le répertoire de fichier privé**. Par défaut, les fichiers des utilisateur⋅ices qui ne doivent pas être accessibles publiquement sont stockés dans l'arborescence de fichier du site. Cela peut poser un problème de sécurité : il est conseillé de les placer dans un dossier qui n'est pas directement accessible par Internet.  Pour cela, créez un dossier sur votre serveur _en dehors_ de votre racine Framaforms (`/var/www/framaforms` ou autre), puis rendez-vous à l'adresse `/admin/config/media/file-system`et renseignez le chemin de ce dossier. Si vous obtenez une erreur, assurez-vous que ce dossier est accessible par l'utilisateur du reverse-proxy (`www-data` pour NGINX), et que ce dossier est accessible en écriture.
* **Modifier la langue de votre site** : Pour installer Framaforms dans une autre langue, [suivez ces instructions](https://www.drupal.org/docs/7/install/install-drupal-in-another-language#s-installing-a-language-after-installing-drupal ). ([Comment participer à la traduction de Framaforms?]({filename}../Contributing/contributing-fr.md))

# Pour les curieux⋅ses

## Qu'est-ce qu'un profil d'installation Drupal ?

Un profil d'installation Drupal permet simplement d'installer un groupe de module et de thèmes en même temps que Drupal afin de donner tout de suite accès à certains fonctionnalités. Il contient un script d'installation qui s'exécute à l'installation de Drupal, ainsi que toutes les dépendances nécessaires (modules, thèmes).

Dans notre cas, nous utilisons le profil `framaforms_org` pour embarquer tous les modules Drupal nécessaires (notamment les modules `Webform` et  `Form builder`, mais pas que !), ainsi que des thèmes et du contenu statique (images, modèles HTML...)

## Qu'est-ce qu'une `feature` Drupal ?

Une `feature` Drupal est une manière de rapidement configurer un site (et tous ses modules) avec des valeurs par défaut. La configuration de Drupal se trouve dans sa base de données, ce qui veut dire qu'avec un nouveau site, il faudrait recommencer toute la configuration. Mais à l'aide d'une `feature`, plus besoin.

Framaforms fonctionne avec une unique `feature`, nommée sobrement `framaforms_feature`, qui contient toute la configuration nécessaire pour Framaforms : les vues qui permettent de visualiser des listes de contenu, les permissions associées aux différents rôles...
