Title: Install
Authors: moi
Date: 2020-06-14
Modified: 2020-06-14
Slug: install
Lang: en

# Requirements
* **PHP 7.3**, as recommanded for Drupal 7, along with the following extensions : `mb_string`, `pgsql` and `dom` (simply run `sudo apt-get install php7.3-yourmodule` on debian-based systems). The `curl` module can also be useful for development and testing purposes.
* **NGINX**. May work with Apache, but we haven't tried. If you managed to run it, let us know !
* **PostgreSQL 9.4.12** or later.

_**Note**: Postgres can look like a non-standard choice, and Drupal supports both MySQL and Postgres. This choice was made for performance reasons, MySQL not being able to scale for heavily-used instances. Some queries might fail if you try to use MySQL, especially those regarding the [expiration of webforms](https://framagit.org/framasoft/framaforms/-/wikis/Expiration-of-webforms), and the corresponding features might be affected. If you managed to install Framaforms using MySQL and it's working fine for you, let us know as well !_

# Preparation

You'll need to configure your **reverse proxy** to make Drupal available to the outside world.

Here's an example of a working NGINX configuration:

```
server {
    listen 80;
    listen [::]:80;

    root /var/www/your_folder;
    index index.html;

    location ~ \.php$ {
        try_files $uri =404;
        include /etc/nginx/fastcgi_params;
        fastcgi_send_timeout 300s;
        fastcgi_read_timeout 300s;
        fastcgi_pass unix:/run/php/php7.3-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    }

    error_log /var/log/nginx/<yoursite>.error.log;
	access_log /var/log/nginx/<yoursite>.access.log combined;

    location / {
        autoindex on ;
        autoindex_exact_size off;
        try_files $uri index.html /index.php?$query_string;
    }
}
```

_**NB: you'll need to [add your SSL configuration](https://www.nginx.com/blog/using-free-ssltls-certificates-from-lets-encrypt-with-nginx/) to this server block to complete a secure installation.** You'll want to run `nginx -t` before restarting NGINX to test your configuration._

All good? Then we can go on to the actual installation!

# Installation steps
1. **Download Drupal 7**. Go to [the Drupal website](https://www.drupal.org/project/drupal) and download the latest 7.x version of Drupal.
2. **Place the distribution in your web directory.** This can be anywhere on your machine, where you're pointing your HTTP server.
3. **Download the Framaforms installation profile.** [Download the latest release of the profile]({filename}../Download/download-en.md) and copy the folder into the `profiles/` subfolder of your Drupal distribution. You should now have a folder under `profiles/framaforms_org` containing all Framaforms-specific content.
4. **Set up your database and `settings.php` file** as instructed by the [Drupal website](https://www.drupal.org/docs/7/install) (steps 2 and 3).
5. **Access the installation script.** Browse to your website at the following address: `<yoururl.example>/install.php`. You'll be presented with an installation menu. Wait for everything to install and ...
   ![Installation menu]({static}../../images/install_installation_pannel.png)
6. **Fill in your website information.** There you'll need to set up your site name, email address, and administration account. This is account that you'll use to manage Framaforms, so be sure to memorize the login and password.
  ![Configuration pannel]({static}../../images/install_config_pannel.png)
7. Save, and visit your website. **You're almost there!** There's just two more things you'll need to do.
8. **Activate the feature**. Click on "Modules" in the administration bar (or browse to `/admin/menu`), look for the
  **"framaforms_feature"** line. Check the corresponding box, and save your configuration by clicking on the submit button at the bottom of the page.
  ![Activate feature menu]({static}../../images/install_save_feature.png)
  _Activate the feature_!
9. **(optional) Create default pages.** This you can do yourself later, but Framaforms comes with default pages for : index (the frontpage of your site), 404 and 403 pages, and user documentation. In order to do that, browse to `/admin/config/system/framaforms`  and click on the "Create default page" button at the bottom of the page.

And you should be good to go!

# Further tweaking (optional)
Framaforms comes with a default configuration, that you can entirely change to your liking, by accessing all the Drupal core and modules configuration menus.

Some things that you might want to changes can include :

* **hide site errors** : site errors, warnings and messages are visible to all users, but you might want to hide them. For that, you'll need to browse to `/admin/people/permissions`, and disable the corresponding lines for authenticated and/or anonymous users (" View status messages", "View warning messages", "View error messages").

* **change the private file system path**: for now, private user files are stored inside the root folder. That is not good practice : for security reasons, you should separate files that are accessible to everyone (pictures for the homepage and documentation, etc) to user-specific files that should be kept secret (such as a form's export .csv files). In order to do that, you'll need to browse to create the private folder _outside_ of the web directory, then browse to `/admin/config/media/file-system` and fill in the folder system path. If you get errors, make sure your folder's owner is you HTTP server user, and that the folder is writable.
* **change the language of your website**: note that for now **providing a translation file during the installation process causes it to crash**. This issue hasn't been fixed yet. That means that your website will be installed in English by default. But you can change the language _after_ having installed Drupal following [these instructions](https://www.drupal.org/docs/7/install/install-drupal-in-another-language#s-installing-a-language-after-installing-drupal).

# If you're curious

## What is an Drupal installation profile ?
As stated in [the Drupal documentation](https://www.drupal.org/docs/7/install/using-an-installation-profile):
> Installation profiles provide site features and functions for a specific type of site as a single download containing Drupal core, contributed modules, themes, and pre-defined configuration.

An installation profile basically allows you to install various modules and themes on your Drupal installation, and make sure you have all the feature you need for a specific purpose. It has an installation script that is run on Drupal installation, dependencies, etc.

In our case, the `framaforms_org` profile packages the necessary Drupal modules (most notably `webform` and `form_builder` but not only!), themes (the `frama` theme was created specifically for this website), and assets such as static images.

## What is a Drupal feature?
As stated in [the Drupal documentation](https://www.drupal.org/project/features):
> Features provides a UI and API for taking different site building components from modules with exportables and bundling them together in a single feature module.

Basically, Drupal stores all its configuration in the database, which is not ideal for the installation process. `features` is a Drupal module that transforms this configuration into code. A feature then behaves like a module.

Framaforms has a single `feature`, named `framaforms_feature`, that provides the necessary configuration for a lot of modules (for example : some `views` are defined in this feature, user permissions are set, etc).
